<?php include('includes/meta.php'); ?>
	<!--header-->
       <?php include('includes/header.php'); ?>
        <!--banner-->
        <?php// include('includes/banner.php'); ?>

        <div id="main" class="inner">
        	<div class="container">
                <!--content-->
                <div id="content">
                    <article>
                        <h1>SERVICES</h1>
                        <div class="serv-box" id="fire-training">
                            <!-- <iframe src="https://www.youtube.com/embed/y487ALbAvho?feature=player_embedded" frameborder="0" allowfullscreen></iframe> -->
                            <a target="_blank" href="https://www.youtube.com/embed/y487ALbAvho"><img src="images/content/vid.jpg" alt=""></a>
                            <h3>Quick Fire Training Video</h3>
                            <p>We offer fire training classes at a very affordable rate! This is a sample video of just how easy anyone can extinguish a fire with a fire extinguisher. Property of: (Tomelval)</p>
                            <h2>FIRE TRAINING CLASSES</h2>
                            <p>We Offer 2 types of training classes. One is simply a video and brief discussion about fire extinguishers and how they are used. The second option is a hands-on training class where participants actually get to put out a small to medium gasoline pan fire to get the real experience of using an actual fire extinguisher. We can combine both types of training into one class if both are desired.  </p>
                            <h6><span>P</span>- pull (the ring pin)<br/>
                           <span>A</span>- aim (the hose)<br/>
                           <span>S</span>- squeeze (the handle)<br/>
                           <span>S</span>- sweep (side to side)</h6>
                           <div class="services-pics">
                               <img src="images/content/s1-1.jpg" alt="">
                               <img src="images/content/s1-2.jpg" alt="">
                               <img src="images/content/s1-3.jpg" alt="">
                               <img src="images/content/s1-4.jpg" alt="">
                               <img src="images/content/s1-5.jpg" alt="">
                           </div>
                        </div>
                        <div class="serv-box" id="fire-triangle">
                            <h2>THE  " FIRE TRIANGLE "</h2>
                            <img src="images/content/s2-1.jpg" class="serv-left" alt="">
                            <p>Fire is a very rapid chemical reaction between oxygen and a combustible material, which results in the release of heat, light, flames, and smoke. </p>
                            <p>For fire to exist, the following FOUR elements must be present at the same time:</p>
                            <p>1) Enough oxygen to sustain combustion<br/>
                            2) Enough heat to raise the material to its ignition temperature<br/>
                            3) Some sort of fuel or combustible material<br/>
                            4) The chemical reaction that is fire. </p>
                            <br class="clear"/>
                            <div class="services-pics">
                               <img src="images/content/s2-2.jpg" alt="">
                               <img src="images/content/s2-3.jpg" alt="">
                           </div>
                        </div>
                        <div class="serv-box" id="fire-types">
                            <h2>TYPES OF FIRE EXTINGUISHERS</h2>
                            <img src="images/content/s3-1.jpg" class="fire-inline" alt="">
                            <img src="images/content/s3-2.jpg" class="fire-inline2" alt="">
                            <br/>
                            <img src="images/content/s3-3.jpg" class="fire-type" alt="">
                            <h4>Water Extinguishers</h4>
                            <p>or APW extinguishers (air-pressurized water) are suitable for CLASS A fires only. <i>NEVER USE A WATER EXTINGUISHER ON A GREASE FIRE</i>, electrical fires or class D fires - the flames will spread and make the fire bigger. Water extinguishers are filled with water and are typically pressurized with air. </p>
                            <img src="images/content/s3-4.jpg" class="fire-type" alt="">
                            <h4>Carbon Dioxide (CO2) Extinguishers</h4>
                            <p>are used for <strong>CLASS B and CLASS C</strong> fires. CO2 extinguishers contain carbon dioxide, a non-flammable gas, and are highly pressurized. The pressure is so great that it is not uncommon for bits of dry ice to shoot out the nozzle. They don't work very well on class A fires because they may not be able to displace enough oxygen to put the fire out, causing it to re-ignite.</p>
                            <img src="images/content/s3-5.jpg" class="fire-type" alt="">
                            <h4>Dry Chemical Extinguishers</h4>
                            <p>come in a variety of types and are suitable for a combination of <strong>CLASS A, CLASS B and CLASS C fires</strong>. These are filled with foam or powder and pressurized with nitrogen.</p>
                            <ul>
                                <li><strong>BC</strong> - This is the regular type of dry chemical extinguisher. It is filled with sodium bicarbonate or potassium bicarbonate. The BC variety leaves a mildly corrosive residue which must be cleaned immediately to prevent any damage to materials. </li>
                                <li><strong><span>ABC - This is the multipurpose dry chemical extinguisher</span></strong>. The ABC type is filled with monoammonium phosphate, a yellow powder that leaves a sticky residue that may be damaging to electrical appliances such as a computer. ~MOST COMMON EXTINGUISHER IN ANY BUSINESS OR HOME~</li>
                            </ul>
                            <img src="images/content/s3-6.jpg" class="fire-type" alt="">
                            <h4>Halotron Extinguishers</h4>
                            <p>HALOTRON is known as a Clean Agent that is discharged as a rapidly evaporating liquid which leaves no residue. It effectively extinguishes Class A and B fires by cooling and smothering and it will not conduct electricity back to the operator. Halotron is pressurized with Argon gas and is an EPA and FAA approved agent suitable for use on Class A, B and C fires. Halotron extinguishers are intended for use in areas formerly protected by Halon 1211 portable extinguishers such as in computer rooms, telecommunications facilities, clean rooms, data storage areas, offices, boats and vehicles.</p>
                            <img src="images/content/s3-7.jpg" class="fire-type" alt="">
                            <h4>Wet Chemical (K-Class) Extinguishers</h4>
                            <p>are the best restaurant kitchen appliance fire extinguishers you can purchase. They contain a special potassium acetate based, low PH agent developed for use in pre-engineered restaurant kitchen systems. The recent trend to more efficient cooking appliances and use of unsaturated cooking oils dictates the use of a  portable fire extinguisher with greater fire fighting capacity and cooling effect to combat these very hot and difficult fires.</p>
                            <img src="images/content/s3-8.jpg" class="fire-type" alt="">
                            <h4>Purple K  Extinguishers</h4>
                            <p>contain specially fluidized and siliconized potassium bicarbonate dry chemical which is particularly effective on Class B flammable liquids and pressurized gases and it is electrically nonconductive.</p>
                            <img src="images/content/s3-9.jpg" class="fire-type" alt="">
                            <h4>Foam Extinguishers</h4>
                            <p>dual rating for types 'A' &amp; 'B' fires mark the Foam Extinguisher's ability to contain and put out fires from combustible materials and flammable liquids. The Foam Extinguisher contains an Aqueous Film Forming Foam (AFFF). When sprayed upon a flame the chemical foam spreads on the flame covered materials and liquids and forms a film on it which neutralizes the burning of the material. Since it is made of nitrogen the foam then cools the fires or materials on fire.</p>
                        </div>
                        <div class="serv-box" id="fire-service">
                            <h2>FIRE EXTINGUISHER SERVICE</h2>
                            <p>At RED <span>E</span> Fire Protection, we strive to excel at our customer service by contacting your business when it is time for the annual inspection of your fire extinguishers and promptly respond to all service calls or inquiries. </p>
                            <p>Services provided to our local customers:</p>
                            <ul>
                                <li>Fire Extinguisher Service / Recharges</li>
                                <li>Fire Extinguisher Sales / Installations</li>
                                <li>Fire Extinguisher Repairs / Inspections</li>
                                <li>Hydrostatic Testing</li>
                                <li>Cabinet Sales / Installations</li>
                                <li>Also, Emergency Lighting Maintenance</li>
                            </ul>
                            <p>Most servicing can be performed on-site. We carry all commonly used extinguisher agents, as well as parts for all makes and models of extinguishers to help expedite repairs. If your extinguisher needs to be taken to our facility for repairs, we will loan you a like model until yours is returned, if your extinguisher cant be repaired we will offer you a new model at a discounted price. </p>
                            <p>All extinguishers sold on the local level will automatically be scheduled for annual service as a courtesy to our customers. A service schedule can be provided for your extinguisher upon request.</p>
                            <p>CALL US AT RED <span>E</span> FIRE PROTECTION TODAY TO SCHEDULE SERVICE !!</p>
                            <p><a href="tel:702-767-6851">(702) 767 - 6851 </a></p>
                            <div class="services-desc">
                               <img src="images/content/s4-1.jpg" alt="">
                               <h5>GAUGE IS IN THE <span class="green">GREEN</span> - "It's RED <span class="red">E</span>"</h5>
                            </div>
                            <div class="services-desc">
                               <img src="images/content/s4-2.jpg" alt="">
                               <h5>GAUGE IS NOT IN THE <span class="green">GREEN THEN...</span></h5>
                            </div>
                            <div class="services-desc">
                               <img src="images/content/s4-3.jpg" alt="">
                               <h5><span class="red">RECHARGE IT IMMEDIATELY</span></h5>
                            </div>
                            <div class="services-desc">
                               <img src="images/content/s4-4.jpg" alt="">
                           </div>
                           <br/>
                           <p>TO PUT A REQUEST IN FOR SERVICE, PLEASE CLICK THE FOLLOWING LINK AND FILL IN THE REQUIRED FIELDS AND SUBMIT IT TO US AND A SERVICE REP WILL CONTACT YOU. <a href="contact.php">SERVICE REQUEST FORM</a></p>
                        </div>
                        <div class="serv-box" id="fire-osha">
                            <h2>OSHA REQUIREMENTS FOR PORTABLE FIRE EXTINGUISHERS (1910.157)</h2>
                            <p>( for more info, visit the <a target="_blank" href="osha.gov">OSHA</a> site )</p>
                            <p><strong>1910.157(c)(1)</strong><br/>
                            The employer shall provide portable fire extinguishers and shall mount, locate and identify them so that they are readily accessible to employees without subjecting the employees to possible injury.</p>
                            <p><strong>1910.157(c)(2)</strong><br/>
                            Only approved portable fire extinguishers shall be used to meet the requirements of this section.</p>
                            <p><strong>1910.157(c)(3)</strong><br/>
                            The employer shall not provide or make available in the workplace portable fire extinguishers using carbon tetrachloride or chlorobromomethane extinguishing agents.</p>
                            <p><strong>1910.157(c)(4)</strong><br/>
                            The employer shall assure that portable fire extinguishers are maintained in a fully charged and operable condition and kept in their designated places at all times except during use.</p>
                            <p><strong>1910.157(c)(5)</strong><br/>
                            The employer shall remove from service all soldered or riveted shell self-generating soda acid or self-generating foam or gas cartridge water type portable fire extinguishers which are operated by inverting the extinguisher to rupture the cartridge or to initiate an uncontrollable pressure generating chemical reaction to expel the agent.</p>
                            <p><strong>1910.157(d)</strong><br/>
                            Selection and distribution.</p>
                            <p><strong>1910.157(d)(1)</strong><br/>
                            Portable fire extinguishers shall be provided for employee use and selected and distributed based on the classes of anticipated workplace fires and on the size and degree of hazard which would affect their use.</p>
                            <p><strong>1910.157(d)(2) 1910.157(d)(2)</strong><br/>
                            The employer shall distribute portable fire extinguishers for use by employees on Class A fires so that the travel distance for employees to any extinguisher is 75 feet (22.9 m) or less.</p>
                            <p><strong>1910.157(d)(3)</strong><br/>
                            The employer may use uniformly spaced standpipe systems or hose stations connected to a sprinkler system installed for emergency use by employees instead of Class A portable fire extinguishers, provided that such systems meet the respective requirements of 1910.158 or 1910.159, that they provide total coverage of the area to be protected, and that employees are trained at least annually in their use.</p>
                            <p><strong>1910.157(d)(4)</strong><br/>
                            The employer shall distribute portable fire extinguishers for use by employees on Class B fires so that the travel distance from the Class B hazard area to any extinguisher is 50 feet (15.2 m) or less.</p>
                            <p><strong>1910.157(d)(5)
                            The employer shall distribute portable fire extinguishers used for Class C hazards on the basis of the appropriate pattern for the existing Class A or Class B hazards.</p>
                            <p><strong>1910.157(d)(6)</strong><br/>
                            The employer shall distribute portable fire extinguishers or other containers of Class D extinguishing agent for use by employees so that the travel distance from the combustible metal working area to any extinguishing agent is 75 feet (22.9 m) or less. Portable fire extinguishers for Class D hazards are required in those combustible metal working areas where combustible metal powders, flakes, shavings, or similarly sized products are generated at least once every two weeks.</p>
                            <p><strong>1910.157(e)</strong><br/>
                            Inspection, maintenance and testing.</p>
                            <p><strong>1910.157(e)(1)</strong><br/>
                            The employer shall be responsible for the inspection, maintenance and testing of all portable fire extinguishers in the workplace.</p>
                            <p><strong>1910.157(e)(2)</strong><br/>
                            Portable extinguishers or hose used in lieu thereof under paragraph (d)(3) of this section shall be visually inspected monthly.</p>
                            <p><strong>1910.157(e)(3)</strong><br/>
                            The employer shall assure that portable fire extinguishers are subjected to an annual maintenance check. Stored pressure extinguishers do not require an internal examination. The employer shall record the annual maintenance date and retain this record for one year after the last entry or the life of the shell, whichever is less. The record shall be available to the Assistant Secretary upon request.</p>
                            <p><strong>1910.157(e)(4)</strong><br/>
                            The employer shall assure that stored pressure dry chemical extinguishers that require a 12-year hydrostatic test are emptied and subjected to applicable maintenance procedures every 6 years. Dry chemical extinguishers having non-refillable disposable containers are exempt from this requirement. When recharging or hydrostatic testing is performed, the 6-year requirement begins from that date.</p>
                            <p><strong>1910.157(e)(5)</strong><br/>
                            The employer shall assure that alternate equivalent protection is provided when portable fire extinguishers are removed from service for maintenance and recharging.</p>
                            <p><strong>1910.157(f)</strong><br/>
                            Hydrostatic testing.</p>
                            <p><strong>1910.157(f)(1)</strong><br/>
                            The employer shall assure that hydrostatic testing is performed by trained persons with suitable testing equipment and facilities.</p>
                            <p><strong>1910.157(f)(3)</strong><br/>
                            In addition to an external visual examination, the employer shall assure that an internal examination of cylinders and shells to be tested is made prior to the hydrostatic tests.</p>
                            <p><strong>1910.157(f)(4)</strong><br/>             
                            The employer shall assure that portable fire extinguishers are hydrostatically tested whenever they show new evidence of corrosion or mechanical injury, except under the conditions listed in paragraphs (f)(2)(i)-(v) of this section.</p>
                            <p><strong>1910.157(f)(5)</strong><br/>
                            The employer shall assure that hydrostatic tests are performed on extinguisher hose assemblies which are equipped with a shut-off nozzle at the discharge end of the hose. The test interval shall be the same as specified for the extinguisher on which the hose is installed.</p>
                            <p><strong>1910.157(f)(6)</strong><br/>
                            The employer shall assure that carbon dioxide hose assemblies with a shut-off nozzle are hydrostatically tested at 1,250 psi (8,620 kPa).</p>
                            <p><strong>1910.157(f)(7)</strong><br/>
                            The employer shall assure that dry chemical and dry powder hose assemblies with a shut-off nozzle are hydrostatically tested at 300 psi (2,070 kPa).</p>
                            <p><strong>1910.157(f)(8)</strong><br/>
                            Hose assemblies passing a hydrostatic test do not require any type of recording or stamping.</p>
                            <p><strong>1910.157(f)(9)</strong><br/>
                            The employer shall assure that hose assemblies for carbon dioxide extinguishers that require a hydrostatic test are tested within a protective cage device.</p>
                            <p><strong>1910.157(f)(10)</strong><br/>
                            The employer shall assure that carbon dioxide extinguishers and nitrogen or carbon dioxide cylinders used with wheeled extinguishers are tested every 5 years at 5/3 of the service pressure as stamped into the cylinder. Nitrogen cylinders which comply with 49 CFR 173.34(e)(15) may be hydrostatically tested every 10 years.</p>
                            <p><strong>1910.157(f)(11)</strong><br/>
                            The employer shall assure that all stored pressure and Halon 1211 types of extinguishers are hydrostatically tested at the factory test pressure not to exceed two times the service pressure.</p>
                            <p><strong>1910.157(f)(12)</strong><br/>
                            The employer shall assure that acceptable self-generating type soda acid and foam extinguishers are tested at 350 psi (2,410 kPa).</p>
                            <p><strong>1910.157(f)(13)</strong><br/>
                            Air or gas pressure may not be used for hydrostatic testing.</p>
                            <p><strong>1910.157(f)(14)</strong><br/>
                            Extinguisher shells, cylinders, or cartridges which fail a hydrostatic pressure test, or which are not fit for testing shall be removed from service and from the workplace.</p>
                            <p><strong>1910.157(f)(15)(i)</strong><br/>
                            The equipment for testing compressed gas type cylinders shall be of the water jacket type. The equipment shall be provided with an expansion indicator which operates with an accuracy within one percent of the total expansion or .1cc (.1mL) of liquid.</p>
                            <p><strong>1910.157(f)(15)(ii)</strong><br/>
                            The equipment for testing non-compressed gas type cylinders shall consist of the following:</p>
                            <p><strong>1910.157(f)(15)(ii)(A)</strong><br/>
                            A hydrostatic test pump, hand or power operated, capable of producing not less than 150 percent of the test pressure, which shall include appropriate check valves and fittings;</p>
                            <p><strong>1910.157(f)(15)(ii)(B)</strong><br/>
                            A flexible connection for attachment to fittings to test through the extinguisher nozzle, test bonnet, or hose outlet, as is applicable; and</p>
                            <p><strong>1910.157(f)(15)(ii)(C)</strong><br/>
                            A protective cage or barrier for personal protection of the tester, designed to provide visual observation of the extinguisher under test.</p>
                            <p><strong>1910.157(f)(16)</strong><br/>
                            The employer shall maintain and provide upon request to the Assistant Secretary evidence that the required hydrostatic testing of fire extinguishers has been performed at the time intervals shown in Table L-1. Such evidence shall be in the form of a certification record which includes the date of the test, the signature of the person who performed the test and the serial number, or other identifier, of the fire extinguisher that was tested. Such records shall be kept until the extinguisher is hydrostatically retested at the time interval specified in Table L-1 or until the extinguisher is taken out of service, whichever comes first.</p>
                            <p><strong>1910.157(g)</strong><br/>
                            Training and education.</p>
                            <p>1910.157(g)(1)
                            Where the employer has provided portable fire extinguishers for employee use in the workplace, the employer shall also provide an educational program to familiarize employees with the general principles of fire extinguisher use and the hazards involved with incipient stage fire fighting.</p>
                            <p><strong>1910.157(g)(2)</strong><br/>
                            The employer shall provide the education required in paragraph (g)(1) of this section upon initial employment and at least annually thereafter.</p>
                            <p><strong>1910.157(g)(3)</strong><br/>
                            The employer shall provide employees who have been designated to use fire fighting equipment as part of an emergency action plan with training in the use of the appropriate equipment.</p>
                            <p><strong>1910.157(g)(4)</strong><br/>
                            The employer shall provide the training required in paragraph (g)(3) of this section upon initial assignment to the designated group of employees and at least annually thereafter. </p>
                            <br/>
                            <p>[45 FR 60708, Sept. 12, 1980; 46 FR 24557, May 1, 1981, as amended at 51 FR 34560, Sept. 29, 1986; 61 FR 9227, March 7, 1996; 67 FR 67964, Nov. 7, 2002]</p>
                            <p>OSHA.</p>
                        </div>
                        <div class="serv-box" id="fire-regulations">
                            <h2>CODES &amp; REGULATIONS FOR NEVADA</h2>
                            <img src="images/content/s5-1.jpg" class="serv-left" alt="">
                            <h3>NFPA 10 STD For Portable Fire Extinguishers</h3>
                            <br/>
                            <p><a href="">NFPA 10 (Portable Fire Extinguishers)  </a></p>
                            <h3>Fire Extinguisher Inspections In The State Of Nevada...</h3>
                            <br/>
                            <p>Nevada used to conduct inspections every three years. Inspections are now done annually to ensure the safety of employees in businesses.Maintenance must be performed annually to ensure that all parts are in working order. Extinguishers are opened and the powder checked. Recharging is done once per year. The company performing the service tags the fire extinguisher with the date of service, license number and signature. These tags must be approved by the State Fire Marshall prior to being used. A collar punched with the current year is affixed to the fire extinguisher. The manufacturer's label on the fire extinguisher must remain in clear sight. Business owners are required to do visual checks of all fire extinguishers monthly. If a new portable fire extinguisher has been purchased a receipt must be kept to prove the purchase date.</p>
                            <br class="clear"/>
                            <h3>Americans with Disabilities Act (ADA) </h3>
                            <p>Guidelines for Fire Extinguishers, Cabinets, and Fire Hose/Valve Cabinets</p>
                            <p>The following guidelines should be used when fire extinguisher cabinets, fire hose/valve cabinets, and other fire protection cabinets are located in public accommodations and commercial facilities subject to Title III of the American with Disabilities Act (ADA)</p>
                            <p><strong>Wall Projections:</strong><br/>ADA Accessibility Guidelines (ADAAG) specify that objects projecting from walls with their leading edges between 27" and 80" above the finished floor shall protrude no more than 4" into walks, corridors, passageways, or aisles. Objects mounted with their leading edges at or below 27" may protrude any amount. </p>
                            <p><strong>Mounting Heights:</strong><br/>ADA Guidelines specify reach ranges for building occupants who require access to equipment such as fire extinguishers and other fire safety devices. For an unobstructed approach, the maximum forward reach is 48” above the floor if a front approach to the equipment is the only option for accessing the equipment.  For example, the fire extinguisher top handle is 48" above the floor.  54” is the maximum height if a side approach is possible. </p>
                            <p>The actual mounting heights for cabinets housing this equipment can be determined by reviewing the exact dimensions of the specified cabinet. </p>
                        </div>
                        <div class="serv-box" id="fire-lights">
                            <h2>EMERGENCY / EXIT LIGHTING  " E-Lights "</h2>
                            <img src="images/content/s6-1.jpg" class="serv-left" alt="">
                            <h3>Is your Company prepared for an emergency?</h3>
                            <p>In case the unthinkable happens, efficient exit and emergency lighting systems can save many lives, including your own. Exit and emergency lights also increase your security and comfort and are required by law in many cases. Avoid serious liability and expensive fines by relying on our Exit and Emergency lighting solutions. Simple maintenance and inspections from new light bulbs, batteries, annual tests, monthly inspections and additional glow in the dark pathways, landings and additional luminous signs near doorways will help keep everyone safe and up to code.</p>
                            <p><a target="_blank" href="http://www.redelights.biz/">VISIT OUR NEWEST WEBSITE  RedE Lights.biz and discover more!</a></p>
                            <p><a target="_blank" href="https://www.youtube.com/channel/UC-MAU0NLMzRTupohQoolajg">EMERGENCY LIGHT VIDEO LIBRARY - Click Here!</a></p>
                            <br class="clear"/>
                            <div class="services-pics">
                               <img src="images/content/s6-2.jpg" alt="">
                           </div>
                           <img src="images/content/s6-3.jpg" alt="">
                           <p>Some Regulations and Codes for Emergency/Exit Lighting</p>
                           <p><strong><span>Standard Fire Prevention Code 1999 807.1.4 Exit Illumination and Signs</span></strong> A functional test shall be conducted on every required emergency lighting system at 30-day intervals for a minimum of 30-seconds.</p>
                           <p><strong><span>NFPA 1997 5-9.3 Periodic Testing of Emergency Lighting Equipment</span></strong> An annual test shall be conducted for a 1-1/2 hour duration. Equipment shall be fully functional for the duration of the test.</p>
                           <p><strong><span>5-10.3.1*</span></strong> Every sign required by 5-10.1.2 or 5-10.1.3 shall be suitably illuminated by a reliable light source. Externally and internally illuminated signs shall be visible in both the normal and emergency Lighting mode. </p>
                           <h4>MAKE YOUR WORK PLACE SAFE AND UP TO CODE WITH SIMPLE MAINTENANCE</h4>
                        </div>
                        <div class="serv-box" id="fire-test">
                            <h2>HYDROSTATIC TESTING CHART</h2>
                            <p>per NFPA 10 Standard for Portable Fire Extinguishers</p>
                            <table>
                                <tr>
                                    <th>EXTINGUISHER TYPE</th>
                                    <th>TESTING INTERVAL</th>
                                </tr>
                                <tr>
                                    <td><strong>WATER</strong><br>
                                    (Store Pressure, Loaded Stream, or Anti-Freeze Extinguishers)</td>
                                    <td><strong>5 Years</strong></td>
                                </tr>
                                <tr>
                                    <td><strong>FOAM</strong><br/>
                                    (AFFF/FFFP Extinguishers)</td>
                                    <td><strong>5 Years</strong></td>
                                </tr>
                                <tr>
                                    <td><strong>CO2</strong><br/>
                                    (Carbon Dioxide Extinguishers)</td>
                                    <td><strong>5 Years</strong></td>
                                </tr>
                                <tr>
                                    <td><strong>DRY CHEMICAL</strong><br/>
                                    (Stored Pressure w/ Mild Steel Shells, Brazed, Aluminum, Cartridge or Cylinder Operated Ext's)</td>
                                    <td><strong>12 Years</strong></td>
                                </tr>
                                <tr>
                                    <td><strong>HALOGENATED</strong><br/>
                                    (Halon 1211, Halotron Agent Extinguishers) </td>
                                    <td><strong>12 Years</strong></td>
                                </tr>
                                <tr>
                                    <td><strong>FE-36</strong><br/>
                                    (Ansul Extinguishers)</td>
                                    <td><strong>12 Years</strong></td>
                                </tr>
                            </table>
                            <h3>HYDROSTATIC TESTING DRY CHEMICAL FIRE EXTINGUISHERS</h3>
                            <p>Our service technicians will recommend the best solution for you and your business at the time your extinguishers are due for hydrostatic testing. They will always try to inform the contact a year or two in advance verbally and/or document it on a sales invoice that certain extinguishers are due. In most instances, its always easier, and cheaper to replace the extinguisher with a brand new unit. In 12 years, technology can change and this is a chance to upgrade to the newest fire extinguisher. If the unit was to be hydrotested and fail this test, you will still need to pay for a new replacement unit and also the test.  Any cylinder damage, excessive wear and tear from age, rust, improper use can cause an extinguisher to fail. EXAMPLE 1: Mfg Date 1996 (hydrostatic  test date or replacement=2008)  EXAMPLE 2: Mfg Date 1999 (hydrostatic test date or replacement=2011)  NOTE: Fire Hoses have a 5yr Hydro Test date. Co2, and Halogenated Extinguishers are usually quite costly when purchased new and are recommended to be hydrostatically tested and recharged unless they are damaged.</p>
                            <p>Extinguisher prices can fluctuate due to market conditions and availability</p>
                        </div>
                        <div class="serv-box" id="fire-tags">
                            <h2>Example of our "Certification" tags...</h2>
                            <p>Our tags are approved by the Nevada State Fire Marshals Office. We are fully licensed in Clark county. All of our service technicians have current certificate of registration licenses. We are also a fully insured. (Lic# E-369)</p>
                            <img src="images/content/s7-1.jpg" alt="">
                            <p>Our Emergency Light Inspection Record Sticker</p>
                            <div class="services-pics">
                                <img src="images/content/s7-2.jpg" alt="">
                            </div>
                            <h3>How to actually read a fire extinguisher tag in Nevada:</h3>
                            <ul>
                                <li>The tag is punched the day the actual service was completed (month, day, and year).</li>
                                <li>Extinguisher is up to code for 1 yr following the annual service or recharge.</li>
                                <li>The tag will also help to identify what kind of extinguisher you have and what type of services were performed on it and more. </li>
                                <li>Nevada State Law states that it is mandatory that all portable fire extinguishers located in businesses are to be serviced by a certified technician and licensed company EVERY year.</li>
                            </ul>
                            <p>Examples:</p>
                            <p>If your extinguisher tag is punched:  MAY 4th, 2010 and today s date is June 8th, 2011 YOUR EXTINGUISHER is DUE for an ANNUAL SERVICE. It needs to be recharged. If your extinguisher tag is punched:  JULY 7th, 2009 and today is June 8th, 2010  then YOUR EXTINGUISHER is still current and up to code for another month and is due for an Annual Service soon (by July 31st 2010).  **Just because an extinguisher tag is up to code doesnt mean the extinguisher is.... the gauge on the extinguisher should always read in the "Green" zone. If it reads in the"Red" zone it needs to be recharged or in some instances replaced or parts replaced. <strong>You should call immediately for service.</strong> We sure hope this helps and clarifies any confusion!!</p>
                        </div>
                    </article>
                </div>
                <br class="clear"/>
            </div>
        </div>

        <!--footer-->
        <?php include('includes/footer.php'); ?>