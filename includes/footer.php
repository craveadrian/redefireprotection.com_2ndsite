        <footer>
            <div id="footer">
                <div class="container5">
                    <div class="ftr-right">
                        <div class="inr-left">
                            <h2>NAVIGATION</h2>
                            <ul>
                                <li <?php isActiveMenu("index.php"); ?>><a href="index.php">HOME</a></li>
                                <li <?php isActiveMenu("services.php"); ?>><a href="services.php">SERVICES</a></li>
                                <li <?php isActiveMenu("about.php"); ?>><a href="about.php">ABOUT US</a></li>
                                <li <?php isActiveMenu("gallery.php"); ?>><a href="gallery.php">GALLERY</a></li>
                                <li <?php isActiveMenu("contact.php"); ?>><a href="contact.php">CONTACT US</a></li>
                            </ul>
                        </div>
                        <div class="inr-right">
                            <h2>LOCATION</h2>
                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3216.0301963189777!2d-115.29173208530396!3d36.28731720410189!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80c893519af28b77%3A0x8710df7c8b0a132b!2s6955+N+Durango+Dr%2C+Las+Vegas%2C+NV+89149%2C+USA!5e0!3m2!1sen!2sph!4v1484304612543" style="border:0" allowfullscreen></iframe>
                            <a target="_blank" href="https://www.google.com/maps?ll=36.287313,-115.289543&z=14&t=m&hl=en-US&gl=PH&mapclient=embed&q=6955+N+Durango+Dr+Las+Vegas,+NV+89149+USA">GET DIRECTION</a>
                            <br class="clear"/>
                        </div>
                        <br class="clear"/>
                    </div>
                    <div class="ftr-left">
                        <div class="inner-right">
                            <h2>CONTACT US</h2>
                            <h3 class="tel">PHONE</h3>
                            <h4><a href="tel:702-767-6851">702-767-6851</a></h4>
                            <h3 class="add">ADDRESS</h3>
                            <h5>2707 E. Craig Rd. North <br>Las Vegas, NV 89030</h5>
                            <h3 class="email">EMAIL</h3>
                            <h6><a href="mailto:tomvalenti@Redefireprotection.com">tomvalenti@Redefireprotection.com</a></h6>
                            <h3>MAILING AND PAYMENT ADDRESS</h3>
                            <h5>P.O. Box #752283</h5>
                        </div>
                        <div class="inner-left">
                            <a href="index.php"><img src="images/common/ftr-logo.png" class="ftr-logo" alt=""></a>
                            <h2><a href="tel:702-767-6851">(702) 767 - 6851</a></h2>
                            <p>&copy; <?php echo date("Y"); ?>. RED E FIRE PROTECTION. All Rights Reserved.</p>
                        </div>
                        <br class="clear"/>
                    </div>
                    <br class="clear"/>
                    <p class="silverc-text"><img src="images/common/header_loglo.png" alt="" class="silverc-logo" /> <span><a href="https://silverconnectwebdesign.com/website-development" rel="external">Web Design</a> Done by <a href="https://silverconnectwebdesign.com" rel="external">Silver Connect Web Design</a></span></p>
                </div>
            </div>
        </footer>
<!--- END FOOTER -->
<script type="text/javascript" src="new-default-forms/scripts/sendform.js" id="sendform"></script>
<script src='//www.google.com/recaptcha/api.js?onload=captchaCallBack&render=explicit' async defer></script>
<script>
var captchaCallBack = function() {
 $('.g-recaptcha').each(function(index, el) {
     grecaptcha.render(el, {'sitekey' : '<?php echo CAPTCHAPUBLICKEY ?>'});
 });
};
</script>

<noscript>
  <p class="nojavascript">Your browser does not support JavaScript. Please change your browser settings to enable Javascript.</p>
  </noscript>  
</body>
</html>