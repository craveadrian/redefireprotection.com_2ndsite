<?php require_once('includes/application.php'); ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1" />
  <meta name="apple-mobile-web-app-capable" content="yes" />
  <meta name="apple-mobile-web-app-status-bar-style" content="black" />
  <meta name="format-detection" content="telephone=no" />
	<title><?php echo PAGETITLE; ?></title>
	<link rel="SHORTCUT ICON" href="favicon.ico" type="image/x-icon" />
  <meta name="Description" content="<?php echo PAGEDESCRIPTION; ?>" /> 
  <meta name="Keywords" content="<?php echo PAGEKEYWORDS; ?>" /> 
	<link href="<?php echo REALPATH; ?>styles/styles.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo REALPATH; ?>styles/contact.css" rel="stylesheet" type="text/css" />
  <link href="https://fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900,900i" rel="stylesheet"> 
  <link href="https://fonts.googleapis.com/css?family=Oswald:300,400,700" rel="stylesheet"> 
  <link href="https://fonts.googleapis.com/css?family=Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
  <script type="text/javascript" src="<?php echo REALPATH; ?>scripts/jquery.js"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<script type="text/javascript" src="<?php echo REALPATH; ?>scripts/gen_validatorv31.js"></script>
  <link rel="stylesheet" href="styles/slicknav.css">
  <script src="http://cdnjs.cloudflare.com/ajax/libs/modernizr/2.6.2/modernizr.min.js"></script>
  <script type="text/javascript" src="scripts/jquery.slicknav.js"></script>
  <script type="text/javascript">
  $(document).ready(function(){
      $('#menu').slicknav();
  });
  </script>
    
    <!-- Scripts and styles used in the gallery fancy box -->
        <?php if(CURRENTPAGE == "gallery.php") { ?>   
          <link rel="stylesheet" type="text/css" href="<?php echo REALPATH; ?>styles/gallery.css" media="screen" /> 
           <link rel="stylesheet" type="text/css" href="<?php echo REALPATH; ?>fancybox/source/jquery.fancybox.css?v=2.1.5" media="screen" />
           <script type="text/javascript" src="<?php echo REALPATH; ?>scripts/jquery.pajinate.js"></script>
           <script type="text/javascript" src="<?php echo REALPATH; ?>fancybox/lib/jquery.mousewheel-3.0.6.pack.js"></script>
           <script type="text/javascript" src="<?php echo REALPATH; ?>scripts/jquery.fancybox-1.3.1.js"></script>
           <script type="text/javascript" src="<?php echo REALPATH; ?>fancybox/source/jquery.fancybox.js?v=2.1.5"></script>
            <script type="text/javascript">
                /* Pajinate Script */
                $(document).ready(function (){
                    $('#cat_gal1').pajinate({ num_page_links_to_display : 3, items_per_page : 3 });
                    $('.fancy').fancybox({
                    helpers: {
                    title : {
                    type : 'outside'
                    },
                    overlay : {
                    speedOut : 0
                    }
                    }
                    });
                }); 
            </script>
        <?php } ?>
      <link rel="stylesheet" href="styles/responsive.css"/>
	</head>
<body onload="blankTargetFix();<?php if(CURRENTPAGE == 'contact.php'){ ?>GetMap();<?php } ?>">