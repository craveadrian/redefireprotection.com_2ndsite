<div id="Contact_Box" class="clearfix">

		<p class="Address_Box">
        <strong>SHOP LOCATON:</strong><br />2707 E. Craig Road, Suite D<br>North Las Vegas, NV 89030
    </p>

    <div>
        <p class="Phone_Box">
				<strong>Phone:</strong><br />
				<a href="tel:702-767-6851">702.767.6851</a>
        </p>
        <p class="Email_Box">
            <strong>Email:</strong><br />
            <a href="mailto:Service@redefireprotection.com" rel="external">Service@redefireprotection.com</a>
        </p>
    </div>
		<p class="Address_Box second">
        <strong>PAYMENTS / MAILING </strong><br />PO Box #752283<br>Las Vegas, NV 89136
    </p>
</div>
