<div id="testimonials-section">
    <div class="container3">
        <h3>YELP</h3>
        <h2>REVIEWS</h2>
        <img src="images/common/quote.png" alt="">
        <p>This is great company! We recently rented new offices and when doing a walk through with the Fire Marshall we found our Fire Extinguisher was older than most who will read this, and hadn't been checked or certified since the mid-90's.
        <br><br>
        Needless to say we had to replace it in a hurry so we called this company and within a very short time a nice guy (Vincent) had brought out a new, charged and certified unit and then set up a return visit in about a year to ensure we remain in compliance. It only took minutes.
        <br><br>
        Tom in the office ensured the order went smoothly and the level of customer service sets these folks apart from the rest</p>
        <img src="images/common/stars.png" alt="">
        <h4>-Stuart G.</h4>
        <h5>Henderson, NV</h5>
        <a href="services.php">READ  MORE</a>
    </div>
</div>