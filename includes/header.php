<header>
    <div id="header">
        <div class="container">
            <div class="nav">
              <ul>
                  <li <?php isActiveMenu("index.php"); ?>><a href="index.php">HOME</a></li
                  ><li <?php isActiveMenu("about.php"); ?>><a href="about.php">ABOUT US</a></li
                  ><li <?php isActiveMenu("services.php"); ?>><a href="services.php">SERVICES</a></li
                  ><li <?php isActiveMenu("gallery.php"); ?>><a href="gallery.php">GALLERY</a></li
                  ><li <?php isActiveMenu("contact.php"); ?>><a href="contact.php">CONTACT US</a></li>
              </ul>
            </div>
        </div>
    </div>
</header>