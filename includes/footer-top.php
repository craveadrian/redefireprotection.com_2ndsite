<div id="footer-top">
    <div class="container4">
        <div class="tftr-left">
            <h2><span>CONTACT</span> US</h2>
            <div class="form">
                <p>We look forward to hearing from you! Whether you have a service request, or would like a FREE estimate please contact us today. </p>
                <form action="formtoemailpro.php" method="post" id="contact-form" class="border">
                    <input type="text" name="name" id="name" placeholder="Name:">
                    <input type="text" name="email" id="email" placeholder="Email:">
                    <input type="text" name="phone" id="phone" placeholder="Phone:">
                    <textarea rows="3" cols="20" id="message" name="message" placeholder="Message:"></textarea>
                    <img id="siimage" style="border: 1px solid #000; margin-right: 15px" src="securimage/securimage_show.php?sid=<?php echo md5(uniqid()) ?>" alt="CAPTCHA Image">
                    <a tabindex="-1" style="border-style: none;" href="#" title="Refresh Image" onclick="document.getElementById('siimage').src = 'securimage/securimage_show.php?sid=' + Math.random(); this.blur(); return false"><img src="securimage/images/refresh.png" alt="Reload Image" onclick="this.blur()"></a><br />
                    <input type="text" name="captcha_code" placeholder="Enter Code*:"/>
                    <input type="submit" value="SUBMIT FORM">
                    <!-- <br class="clear"/> -->
                    <div class="clear"></div>
                </form>
                <script type="text/javascript">
                    var frmvalidator = new Validator("contact-form");
                    frmvalidator.addValidation("name","req","Please enter your name.");
                    frmvalidator.addValidation("email","req","Please enter your email address.");
                    frmvalidator.addValidation("email","email","Please enter a valid email.");
                    frmvalidator.addValidation("message","req","Please enter your message.");
                </script>
            </div>
        </div>
        <div class="tftr-right">
            <h2>ABOUT</h2>
            <!-- <h3><span>RED E</span> FIRE PROTECTION</h3> -->
            <h3>RED <span>E</span> FIRE PROTECTION</h3>
            <p>Our company is based on the belief that our customers’ needs are of the utmost importance. Our team is committed to meeting those needs. As a result, a high percentage of our business is from repeat customers and referrals. <br>We would welcome the opportunity to earn your trust and deliver you the best service in the industry. We are a mobile company and come directly to your place of business. We get your business “ RED E ”  up to code!</p>
            <a class="link" href="about.php">LEARN MORE</a>
            <!-- <div class="ftr-soc">
                <h5>FOLLOW US:</h5>
                <ul>
                    <li><a target="_blank" href=""><img src="images/common/soc-fb2.png" alt=""></a></li>
                    <li><a target="_blank" href=""><img src="images/common/soc-gp2.png" alt=""></a></li>
                    <li><a target="_blank" href=""><img src="images/common/soc-tw2.png" alt=""></a></li>
                    <li><a target="_blank" href=""><img src="images/common/soc-in2.png" alt=""></a></li>
                    <li><a target="_blank" href=""><img src="images/common/soc-yt2.png" alt=""></a></li>
                </ul>
            </div> -->
            <div class="cards">
                <h5>WE ACCEPT:</h5>
                <img src="images/common/cards.png" alt="">
            </div>
        </div>
        <br class="clear"/>
    </div>
</div>