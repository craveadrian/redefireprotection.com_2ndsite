<div id="content-bottom">
    <div class="container3">
        <!-- <h2><span>OUR</span> SERVICES</h2> -->
        <h2>OUR SERVICES</h2>
        <p>Red E Fire Protection offers fire extinguisher services throughout Las Vegas, NV, and Clark County. We excel at customer service because we’re a mobile company that will come directly to your business and schedule your annual fire extinguisher inspection to insure your safety. We are dedicated to meeting our customers’ needs when it comes to fire protection.    Red E Fire Protection offers the following services:     Fire extinguisher sales, installations, service &amp; recharges   Fire extinguisher repairs &amp; inspections   Emergency and exit lighting installations &amp; maintenance   Fire extinguisher training classes &amp; demos   Hydrostatic testing   Cabinet sales &amp; installations    We welcome the opportunity to earn your trust and deliver you the best service in the fire protection industry.    </p>
    </div>
    <div class="container2">
        <div class="cb-con">
            <dl>
                <dt><img src="images/common/pic1.png" alt=""></dt>
                <dd>
                    <strong>01</strong>
                    <span>Fire Extinguisher<br>SALES / INSTALLATION</span>
                    <a href="services.php#fire-service">LEARN MORE</a>
                </dd>
            </dl>
            <dl>
                <dt><img src="images/common/pic2.png" alt=""></dt>
                <dd>
                    <strong>02</strong>
                    <span>EXIT Light<br>Inspections </span>
                    <a href="services.php#fire-lights">LEARN MORE</a>
                </dd>
            </dl>
            <dl>
                <dt><img src="images/common/pic3.png" alt=""></dt>
                <dd>
                    <strong>03</strong>
                    <span>Fire TRAINING<br>CLASSES</span>
                    <a href="services.php#fire-training">LEARN MORE</a>
                </dd>
            </dl>
            <dl>
                <dt><img src="images/common/pic4.png" alt=""></dt>
                <dd>
                    <strong>04</strong>
                    <span>Fire Extinguisher<br>Annual Recharges</span>
                    <a href="services.php#fire-types">LEARN MORE</a>
                </dd>
            </dl>
            <a class="cb-link" href="services.php">VIEW ALL SERVICES</a>
        </div>
    </div>
</div>