<?php include('includes/meta.php'); ?>
	<!--header-->
       <?php include('includes/header.php'); ?>
        <!--banner-->
        <?php// include('includes/banner.php'); ?>

        <div id="main" class="inner">
        	<div class="container">
                <!--content-->
                <div id="content">
                    <article>
                        <h1>ABOUT US</h1>
                        <p>Our company is based on the belief that our customers' needs are of the utmost importance. Our team is committed to meeting those needs. As a result, a high percentage of our business is from repeat customers and referrals. </p>
                        <p>We would welcome the opportunity to earn your trust and deliver you the best service in the industry. We are a mobile company and come directly to your place of business. We get your business " RED <span>E</span> "  up to code!</p>
                        <p>Nevada State Fire Marshal License # E-369<br>All Service Technicians have current COR #'s</p>
                        <div class="abt-pics">
                            <div class="abt-left">
                                <img src="images/content/abt1.jpg" class="ab-left" alt="">
                                <div class="ab-right">
                                    <img src="images/content/abt2.jpg" alt="">
                                    <img src="images/content/abt3.jpg" alt="">
                                </div>
                                <br class="clear"/>
                            </div>
                            <div class="abt-right">
                                <img src="images/content/abt4.jpg" alt="">
                                <img src="images/content/abt5.jpg" alt="">
                            </div>
                            <br class="clear"/>
                        </div>
                        <div class="abt-box">
                            <h2>Payment / Mailing Address:</h2>
                            <h3>RED <span>E</span> FIRE PROTECTION<br/>
                            PO BOX # 752283<br/>
                            Las Vegas, NV 89136</h3>
                        </div>
                        <div class="abt-box">
                            <h2>Tele / Fax</h2>
                            <h4><a href="tel:702-767-6851">(702) 767-6851</a> line</h4>
                            <h4><a href="tel:702-462-9913">(702) 462-9913</a> fax</h4>
                        </div>
                        <div class="abt-box">
                            <h2>Hours</h2>
                            <h4>24 HR Emergency<br/>
                            Service Calls Available<br/>
                            <br/>
                            Mon - Fri: 8AM - 4:30PM<br/>
                            Sat &amp; Sun: On Call</h4>
                        </div>
                    </article>
                </div>
                <br class="clear"/>
            </div>
        </div>

        <!--footer-->
        <?php include('includes/footer.php'); ?>