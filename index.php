<?php include('includes/meta.php'); ?>
	<!--header-->
       <?php include('includes/header.php'); ?>
        <!--banner-->
        <?php include('includes/banner.php'); ?>

        <div id="main" class="main">
            <div class="container2">
            <!--content-->
                <div id="content">
                    <article>
                        <!-- <h2>WELCOME TO</h2>
                        <h1>RED <span>E</span> FIRE PROTECTION</h1> -->
                        <img src="images/common/content.jpg" alt="">
                       <!--  <p>We hope you can find everything you need while visiting our site.<br>RED E Fire Protection is focused on providing high-quality service and customer satisfaction! We will do everything we can to meet your expectations to earn your business and trust. If you want to request service for your company’s fire extinguishers,etc please click the  “contact us” link on the right to submit a request for service.</p> -->
                       <p>RED E Fire Protection is focused on providing high-quality service and customer satisfaction! We will do everything we can to meet your expectations to earn your business and trust.</p>
                        <a class="top-link" href="about.php">LEARN MORE</a>
                    </article>
                </div>
                <?php include('includes/sidebar.php'); ?>
                <br class="clear"/>
            </div>
        </div>

        <!-- content bottom -->
        <?php include('includes/content-bottom.php'); ?>

        <!-- team section -->
        <?php include('includes/team-section.php'); ?>

        <!-- testimonials section -->
        <?php include('includes/testimonials-section.php'); ?>

        <!-- product section -->
        <?php include('includes/product-section.php'); ?>

        <!--footer top-->
        <?php include('includes/footer-top.php'); ?>

        <!--footer-->
        <?php include('includes/footer.php'); ?>